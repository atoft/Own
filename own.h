#pragma once
#include <cassert>
#include <cstdint>

#include <iostream>

#if PTR_SAFETY
class ManagedAllocations
{
	template <typename U>
	friend struct Owned;

	template <typename U>
	friend struct Shared;

	template <typename U, std::size_t Count>
	friend struct OwnedArray;

public:
	static void on_exit()
	{
		assert(count == 0 && "A memory leak occurred (was a cycle of Owneds created?).");
	}

private:
	static std::uint64_t count;
};

std::uint64_t ManagedAllocations::count = 0;
#endif

template <typename T>
struct ManagedHeader
{
#if PTR_SAFETY
	std::uint64_t share_count = 0;
#endif
	std::size_t element_count = 0;

private:
	ManagedHeader() = default;
};


template <typename T>
struct Shared
{
	template <typename U>
	friend struct Owned;

	template <typename U, std::size_t Count>
	friend struct OwnedArray;

private:
	explicit Shared(ManagedHeader<T>* managed)
		: managed(managed)
		, offset(0)
	{
		assert(managed != nullptr);
#if PTR_SAFETY
		++(managed->share_count);
#endif
	}

	Shared(ManagedHeader<T>* managed, std::size_t offset)
	: managed(managed)
	, offset(offset)
	{
		assert(managed != nullptr);
#if PTR_SAFETY
		++(managed->share_count);
#endif
	}

public:
	Shared(Shared& other)
	{
		assert(other.managed != nullptr);
		managed = other.managed;
		offset = other.offset;
#if PTR_SAFETY
		++(managed->share_count);
#endif
	}

	Shared& operator=(Shared& other)
	{
		assert(other.managed != nullptr);
		managed = other.managed;
		offset = other.offset;
#if PTR_SAFETY
		++(managed->share_count);
#endif
		return *this;
	}

	Shared(Shared&& other)
	{
		assert(other.managed != nullptr);
		managed = other.managed;
		offset = other.offset;
		other.managed = nullptr;
	}

	~Shared()
	{
		dispose();
	}

	void dispose()
	{
		if (managed != nullptr)
		{
#if PTR_SAFETY
			assert(managed->share_count > 0);
			--(managed->share_count);
#endif
			managed = nullptr;
		}
	}

	T* get_raw()
	{
		assert(managed != nullptr);
		assert(managed->element_count > offset);

		T* base = (T*)(managed + 1);

		std::cout << "Access Shared at offset " << offset << std::endl;
		return &base[offset];
	}

	const T* get_raw() const
	{
		assert(managed != nullptr);
		assert(managed->element_count > offset);

		const T* base = (const T*)(managed + 1);

		std::cout << "Access Shared at offset " << offset << std::endl;
		return &base[offset];
	}

	T* operator->()
	{
		return get_raw();
	}

	const T* operator->() const
	{
		return get_raw();
	}

	T& operator*()
	{
		return *get_raw();
	}

	const T& operator*() const
	{
		return *get_raw();
	}

	// Supporting Shared pointers to values inside an array means that all Shareds are doubled in size
	// as they must be a pointer to the header + offset into the array.
	// It could be optimized to use only a direct pointer to the element if pointer safety is disabled
	// but any safe program would have to pay this memory cost...
	ManagedHeader<T>* managed;
	size_t offset = 0;
};

template <typename T>
void on_disposed(T& value) {}

template <typename T>
struct Owned
{
public:
	Owned()
	{
		managed = (ManagedHeader<T>*)malloc(sizeof(ManagedHeader<T>) + sizeof(T));
		managed->element_count = 1;
#if PTR_SAFETY
		++ManagedAllocations::count;
#endif
	}

	~Owned()
	{
		dispose();
	}

	void dispose()
	{
		if (managed != nullptr)
		{
			on_disposed(*get_raw());

#if PTR_SAFETY
			assert((managed == nullptr || managed->share_count == 0) && "Tried to destroy an Owned, but there are still sharers.");
#endif
			if (managed != nullptr)
			{
				assert(managed->element_count == 1);
				free(managed);
#if PTR_SAFETY
				--ManagedAllocations::count;
#endif
				managed = nullptr;
			}
		}
	}

	Owned(Owned& other)
	{
		assert(other.managed != nullptr);
		std::cout << "Moving Owned" << std::endl;
		managed = other.managed;
		other.managed = nullptr;
	}

	Owned& operator=(Owned& other)
	{
		assert(other.managed != nullptr);
		std::cout << "Moving Owned" << std::endl;
		managed = other.managed;
		other.managed = nullptr;
		return *this;
	}

	Owned(Owned&& other) = delete;

	T* get_raw()
	{
		assert(managed != nullptr && "Tried to dereference an Owned that has been moved.");
		assert(managed->element_count == 1 && "Got an Owned of an Array, memory corruption?");

		T* base = (T*)(managed + 1);

		return base;
	}

	const T* get_raw() const
	{
		assert(managed != nullptr && "Tried to dereference an Owned that has been moved.");
		assert(managed->element_count == 1 && "Got an Owned of an Array, memory corruption?");

		const T* base = (const T*)(managed + 1);

		return base;
	}

	T* operator->()
	{
		return get_raw();
	}

	const T* operator->() const
	{
		return get_raw();
	}

	T& operator*()
	{
		return *get_raw();
	}

	const T& operator*() const
	{
		return *get_raw();
	}

	Shared<T> share()
	{
		assert(managed != nullptr && "Tried to share an Owned that has been moved.");
		return Shared(managed);
	}

private:
	ManagedHeader<T>* managed;
};

template <typename T, std::size_t Count>
struct OwnedArray
{
public:
	OwnedArray()
	{
		managed = (ManagedHeader<T>*)malloc(sizeof(ManagedHeader<T>) + Count * sizeof(T));
		managed->element_count = Count;
#if PTR_SAFETY
		++ManagedAllocations::count;
#endif
	}

	~OwnedArray()
	{
		dispose();
	}

	void dispose()
	{
		if (managed != nullptr)
		{
			for (std::size_t idx = 0; idx < Count; ++idx)
			{
				on_disposed(*get_raw(idx));
			}

#if PTR_SAFETY
			assert((managed == nullptr || managed->share_count == 0) && "Tried to destroy an OwnedArray, but there are still sharers.");
#endif
			if (managed != nullptr)
			{
				assert(managed->element_count == Count);
				free(managed);
#if PTR_SAFETY
				--ManagedAllocations::count;
#endif
				managed = nullptr;
			}
		}
	}

	OwnedArray(OwnedArray& other)
	{
		assert(other.managed != nullptr);
		std::cout << "Moving OwnedArray" << std::endl;
		managed = other.managed;
		other.managed = nullptr;
	}

	OwnedArray& operator=(OwnedArray& other)
	{
		assert(other.managed != nullptr);
		std::cout << "Moving OwnedArray" << std::endl;
		managed = other.managed;
		other.managed = nullptr;
		return *this;
	}

	OwnedArray(OwnedArray&& other) = delete;

private:
	T* get_raw(std::size_t index)
	{
		assert(index < Count);
		assert(managed != nullptr && "Tried to dereference an OwnedArray that has been moved.");

		T* base = (T*)(managed + 1);
		return &base[index];
	}

public:
	T& operator[](std::size_t index)
	{
		return *get_raw(index);
	}

	Shared<T> share(std::size_t index)
	{
		assert(managed != nullptr && "Tried to share an OwnedArray that has been moved.");
		assert(index < Count);
		return Shared(managed, index);
	}

private:
	ManagedHeader<T>* managed;
};


