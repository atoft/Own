#define PTR_SAFETY 1
#include "own.h"

#include <iostream>
#include <optional>

void borrows_an_int(Shared<int> shared)
{
	std::cout << *shared << std::endl;
	
	(*shared)++;
}

void borrows_and_caches_an_int(Shared<int> shared)
{
	static Shared<int> s_shared = shared;
}

void takes_ownership_of_int(Owned<int> owned)
{
	std::cout << *owned << std::endl;
}

struct Node
{
	int value = 0;
	std::optional<Shared<Node>> parent;
	std::optional<Owned<Node>> child;
};

template<>
void on_disposed(Node& node)
{
	std::cout << "Disposing Node " << node.value << std::endl;

	if (node.parent)
	{
		node.parent->dispose();
	}

	if (node.child)
	{
		node.child->dispose();
	}
};

void test_ints()
{
	Owned<int> my_int = Owned<int>();
	*my_int = 3;
	
	borrows_an_int(my_int.share());
	
	// Will assert at end of scope. (Shared outlives Owned)
	// borrows_and_caches_an_int(my_int.share());
	
	// Will assert at end of scope. (Shared outlives Owned)
	// Shared<int> my_shared = my_int.share();
	
	takes_ownership_of_int(my_int);
		
	// Will assert as my_int is no longer a valid ptr.
	// std::cout << *my_int << std::endl;
}

void test_lists()
{
	Owned<Node> parent = Owned<Node>();
	parent->value = 3;
	
	Owned<Node> child = Owned<Node>();
	child->value = 4;
	
	{
		Shared<Node> shared_parent = parent.share();
	
		child->parent = shared_parent;
	}
	
	Owned<Node> child_child = Owned<Node>();
	child_child->value = 5;
	
	{
		Shared<Node> shared_child_parent = child.share();
	
		child_child->parent = shared_child_parent;
	}
	
	child->child = child_child;
	parent->child = child;

	// Creates a cycle - memory leak.
	// parent->child = parent;
}

void test_arrays()
{
	OwnedArray<int, 6> arr = OwnedArray<int, 6>();

	arr[0] = 3;
	arr[1] = 4;
	arr[2] = 5;
	arr[5] = 47;

	std:: cout << arr[0] << arr[1] << arr[2] << std::endl;

	borrows_an_int(arr.share(1));

	borrows_an_int(arr.share(5));

	// Asserts (bounds check)
	// borrows_an_int(arr.share(6));

    // Will assert at end of scope. (Shared outlives OwnedArray)
	// borrows_and_caches_an_int(arr.share(2));
}

int main()
{
	test_ints();
	
	test_lists();
	
	test_arrays();

#if PTR_SAFETY
	ManagedAllocations::on_exit();
#endif
}

