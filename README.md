Experimenting with memory safety.

# References
- [Ownership You Can Count On](https://researcher.watson.ibm.com/researcher/files/us-bacon/Dingle07Ownership.pdf)
- [A new runtime for Nim](https://nim-lang.org/araq/ownedrefs.html)
- [Pointer Ownership Model](https://resources.sei.cmu.edu/asset_files/WhitePaper/2013_019_001_55008.pdf)
- [Undangle: Early Detection of Dangling Pointers in Use-After-Free
and Double-Free Vulnerabilities](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/Undangle.pdf)
  
- [Outperforming Imperative with Pure Functional Languages](https://www.youtube.com/watch?v=vzfy4EKwG_Y)
